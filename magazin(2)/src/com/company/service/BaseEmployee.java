package com.company.service;

import com.company.departments.BaseDepartment;

public class BaseEmployee {

    private String name;
    private boolean free;
    private BaseDepartment department;

    public BaseEmployee(String name, boolean free, BaseDepartment department) {
        this.name = name;
        this.free = free;
        this.department = department;
    }

    public boolean isFree() {
        return free;
    }
}
