package com.company.departments;

import com.company.clients.BaseVisitor;
import com.company.goods.BaseGoods;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public class BaseDepartment {
    private String name;
    ArrayList <BaseGoods> goods;
    ArrayList <BaseEmployee> employees;

    public BaseDepartment(String name, ArrayList<BaseGoods> goods, ArrayList<BaseEmployee> employees) {
        this.name = name;
        this.goods = goods;
        this.employees = employees;
    }

    public void addEmployee(){

    }

    public void addGood() {
    }
}
