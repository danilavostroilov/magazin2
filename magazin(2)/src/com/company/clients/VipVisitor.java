package com.company.clients;



public class VipVisitor extends BaseVisitor {

    private Integer discount;

    public VipVisitor(String name, Integer discount) {
        super(name);
        this.discount = discount;
    }


    public boolean checkDiscount(){
        if (discount > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void buy(){
        if (checkDiscount()){

        } else {
            super.buy();
        }
    }


}
