package com.company.bank;

public class BaseBank {
    private String name;
    private String creditDescription;

    public BaseBank(String name, String creditDescription) {
        this.name = name;
        this.creditDescription = creditDescription;
    }

    public void checkInfo(){

    }

    public void giveCredit(){
    }
}
