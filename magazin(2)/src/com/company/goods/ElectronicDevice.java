package com.company.goods;

import com.company.departments.BaseDepartment;

public class ElectronicDevice extends BaseGoods {
    private String name;
    private boolean hasGuarantee;
    private String price;

    public ElectronicDevice(BaseDepartment department, String name, boolean hasGuarantee, String price) {
        super(department);
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.price = price;
    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }
}
