package com.company.goods;

public class Computer extends ElectronicDevice{

    private String ram;
    private String company;

    public Computer(String name, boolean hasGuarantee, String department, String price, String ram, String company) {
        super(name, hasGuarantee, department, price);
        this.ram = ram;
        this.company = company;
    }

    public void on(){

    }
    public void off(){

    }
    public void loadOS(){

    }


}
